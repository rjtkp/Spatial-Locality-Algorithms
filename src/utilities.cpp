#include "utilities.h"

double gamma(int S, int dS) {
  if (dS == -1) {
    return 2. * (S + 1) * sqrt((2 * S + 1) * (2 * S + 3));
  } else if (dS == 0) {
    return 2 * S * (S + 1);
  } else if (dS == 1) {
    return 2 * S * sqrt(4 * S * S - 1);
  } else {
    printf("The selection rules has been voilated\n");
  }
  return 0.0;
}

double fmatrix(int dS1, int dS2, int S1, int S2, int J2) {
  // assert dS1 in [-1, 0, 1]
  // assert dS2 in [-1, 0, 1]
  if ((dS1 > 0) || (dS1 == 0 && dS2 > 0)) {
    return fmatrix(-dS1, -dS2, S1 - dS1, S2 - dS2, J2);
  }
  int j1, j2, j3, Sigma;
  // # The fmatrix has symmetry properties, related to the ket<->bra
  // # swap invariance, which can be exploited.
  j1 = S1 + S2 - J2;
  j2 = S1 - S2 + J2;
  j3 = -S1 + S2 + J2;
  Sigma = j1 + j2 + j3;
  if (dS1 == -1) {
    if (dS2 == -1)
      return sqrt((j1 + 2) * (j1 + 1) * (Sigma + 2) * (Sigma + 3));
    else if (dS2 == 0)
      return sqrt((j1 + 1) * (j2 + 1) * j3 * (Sigma + 2));
    else
      return -sqrt((j2 + 1) * (j2 + 2) * (j3 - 1) * j3);
  } else if (dS1 == 0) {
    if (dS2 == -1)
      return -sqrt((j1 + 1) * j2 * (j3 + 1) * (Sigma + 2));
  } else if (dS2 == 0)
    return (j1 * (Sigma + 2) - j2 * j3) / 2.;
  else
    printf("The selection rules were not followed\n");
  return 0.0;
}

double rmatrix(int dS1, int dS2, int S1, int S2, int J2) {
  return -0.50 * fmatrix(dS1, dS2, S1, S2, J2);
}

std::vector<int> build_tree(int height, int tot_spin) {
  std::vector<int> comb;
  std::vector<int> basis;
  std::vector<int> basis1;
  std::vector<int> basis2;

  if (height == 1) {
    return basis;
  }

  int range = 1 << (height - 2);
  range += 1;
  for (int sl = 0; sl < range; sl++) {
    for (int sr = 0; sr < range; sr++) {
      if (abs(sl - sr) <= tot_spin && tot_spin <= sl + sr) {
        // printf("%d\n", sl);
        comb.push_back(sl);
        // printf("%d\n", sr);
        comb.push_back(sr);
      }
    }
  }
  int len = comb.size();

  // a.insert(a.end(), b.begin(), b.end());
  for (int i = 0; i < len; i += 2) {
    basis1 = build_tree(height - 1, comb[i]);
    basis.push_back(comb[i]);
    basis.push_back(comb[i + 1]);
    basis2 = build_tree(height - 1, comb[i + 1]);
    // basis = build_tree(height - 1, comb[i]);
    // for (int i = 0; i < basis1.size(); i++) {
    //   printf("%d %d\n",i, basis1[i]);
    // }
    // for (int i = 0; i < basis2.size(); i++) {
    //   printf("%d %d\n",i, basis2[i]);
    // }
    // basis.insert(basis.end(), build_tree(height - 1, comb[i + 1]).begin(),
    //              build_tree(height - 1, comb[i + 1]).end());
    // build_tree(height - 1, comb[i + 1]);
    for (int j = 0; j < basis1.size(); j++) {
      basis.push_back(basis1[j]);
      basis.push_back(basis2[j]);
    }
    // basis.insert(basis.end(), basis1.begin(),basis1.end());
    // basis.insert(basis.end(), basis2.begin(),basis2.end());
  }
  return basis;
}

// long long int build_basis(long long int *num, std::vector<long long int>
// &basis,
//                  int height, int tot_spin) {
//   if (height == 1) {
//     return basis;
//   }
// int range = 1 << (height - 2);
// range += 1;
// std::vector<int> comb;
// for (int sl = 0; sl < range; sl++) {
//   for (int sr = 0; sr < range; sr++) {
//     if (abs(sl - sr) <= tot_spin && tot_spin <= sl + sr) {
//       comb.push_back(sl);
//       comb.push_back(sr);
//     }
//   }
// }
// int len = comb.size();
//   for (int i = 0; i < len; i += 2) {
//
//   }
// }

// void metropolis(int M, int N, int step, double T, double *en, double *mag,
//                 double *dE, double *spin) {
//   /* given configurations matrix spin, updates state of spin #step according
//   to
//    * the energy deltas */
//   int i;
//   double rr = 0.0;
//   for (i = 0; i < M; ++i) {
//     rr = (double)rand() / RAND_MAX;
//     if (rr < exp(-dE[i] / T)) { // oss: this dE is E_final - E_initial, so we
//                                 // expect configuation to be interesting if
//                                 dE<0
//       spin[i * N + step] = -spin[i * N + step];
//       // update energy
//       en[i] += dE[i];
//       magnetization(spin, mag, N, i);
//       // mag[i]+=2.0/N*spin[i*N+step]; // nb: there might be a - sign here
//     }
//   }
// }
//
// void transpose(double *start, double *end, int N, int M) {
// /* transpose start (N*M) in end (M*N) */
// #ifdef _USE_OMP
// #pragma omp parallel for
// #endif
//   for (int i = 0; i < N; ++i) {
//     for (int j = 0; j < M; ++j) {
//       end[j * N + i] = start[i * M + j];
//     }
//   }
// }
//
