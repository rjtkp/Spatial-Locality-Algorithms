#include <utilities.h> //all the functions are there

// using namespace std;

// #define M_PI 3.14159265
#define K 4
// #define L 8
// #define dim 1430
#define S0 0
#define stride 4
#define verbose False

int main() {
  int dim = 0;
  int L = 1 << (K - 1);
  if (K == 5) {
    dim = 35357670;
  } else if (K == 4) {
    dim = 1430;
  } else if (K == 3) {
    dim = 14;
  }
  printf("dim L %d %d\n", dim, L);

  printf("%f\n", gamma(1, 1));
  long long int num[16];
  long long int y = 15;
  num[0] = 15;
  // for (int i = 1; i < 16; i++) {
  //   printf("x y %lld %lld\n", num[i - 1], y);
  //   y *= 16;
  //   num[i] = num[i - 1] << 4;
  //   printf("shifted x y %lld %lld\n", num[i], y);
  // }
  //
  printf("%f\n", fmatrix(1, 1, 1, 1, 1));
  printf("%f\n", rmatrix(1, 1, 1, 1, 1));

  long long int bra[K - 2];
  long long int ket[K - 2];

  for (int i = 0; i < K - 2; i++) {
    bra[i] = 0;
    ket[i] = 0;
  }
  // int basis_len = (K - 2) * dim;

  std::vector <int> basis;
  // build_basis(num, basis, K-1, S0);
  basis = build_tree(K, 0);
  int len = basis.size();
  printf("%d\n", len);
  for (int i = 0; i < len; i++) {
    printf("%d\n", basis[i]);
  }
  // basis.resize(basis_len);

  // long long int * basis =
  //     (long long int *)calloc(basis_len, sizeof(long long int));

  // printf("y %lld\n", y);
  // y = ~y;
  // printf("y %lld\n", y);

  // gamma(1,1);

  //   int i, j;
  //   double *J, *Ei, *En, *dE, *Mi, *Mf;
  //   double en = 0., mag = 0.;
  //   double t1, t2;
  //
  //   Ei = (double *)calloc(walker, sizeof(double));
  //   dE = (double *)calloc(walker, sizeof(double));
  //   Mi = (double *)calloc(walker, sizeof(double));
  //   Mf = (double *)calloc(walker, sizeof(double));
  //   free(J);
  //   free(dE);
  //   free(En);
  //   free(Mi);
  //   free(Mf);
  return 0;
}
