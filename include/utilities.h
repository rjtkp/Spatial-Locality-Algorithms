#ifndef MOD_H
#define MOD_H

// #include <cblas.h>
// #include <math.h>
// #include <omp.h>
#include <assert.h>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <sys/time.h>
#include <sys/types.h>
#include <vector>
// #include <unistd.h>

double cclock();
double gamma(int s, int ds);
double fmatrix(int dS1, int dS2, int S1, int S2, int J2);
double rmatrix(int dS1, int dS2, int S1, int S2, int J2);
void build_basis(long long int *num, std::vector<long long int> &basis,
                 int height, int tot_spin);
std::vector<int> build_tree(int height, int tot_spin);
// long long int creat_num(long long int num, int i);
// void transpose(double *start, double *end, int N, int M);
// void metropolis(int M, int N, int step, double T, double *en, double *mag,
//                 double *dE, double *sigma);
// void average(double *x,int N, double *out);
// void magnetization(double *sigma, double *mag, int N, int idx);
// void stder(double T,double *data, int N, FILE *file);

#endif
